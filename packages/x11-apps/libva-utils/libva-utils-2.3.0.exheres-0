# Copyright 2017-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=intel ] meson

SUMMARY="Collection of utilities and examples to exercise VA-API"
DESCRIPTION="
libva-utils is a collection of utilities and examples to exercise VA-API in accordance with the
libva project. --enable-tests (default = no) provides a suite of unit-tests based on Google Test
Framework. A driver implementation is necessary to properly operate.
"
HOMEPAGE+=" https://01.org/linuxmedia/vaapi"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    X
    wayland
"

# failing, requires access to video card
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        x11-dri/libdrm[>=2.4]
        x11-libs/libva[>=2.1.0][X?][wayland?]
        X? (
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXfixes
        )
        wayland? ( sys-libs/wayland[>=1.0.0] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-meson-rename-the-h264enc-binary-to-be-in-line-with-t.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddrm=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'X x11'
    'wayland'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

