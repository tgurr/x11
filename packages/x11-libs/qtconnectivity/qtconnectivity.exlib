# Copyright 2017-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtBluetooth and QtNfc"
DESCRIPTION="
* The Bluetooth API provides connectivity between Bluetooth enabled devices.
Qt Bluetooth supports Bluetooth Low Energy development for client/central role
use cases.

* The NFC API provides connectivity between NFC enabled devices. NFC is an
extremely short-range (less than 20 centimeters) wireless technology and has a
maximum transfer rate of 424 kbit/s. NFC is ideal for transferring small
packets of data when two devices are touched together."

LICENCES+="
    LGPL-3
    GPL-2 [[ note = [ sdpcanner, src/tools/sdpscanner/qt_attribution.json ] ]]
"

MYOPTIONS="doc examples"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( x11-libs/qttools:${SLOT}[>=5.11.0] )
    build+run:
        net-wireless/bluez
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
    suggestion:
        net/neard [[
            description = [ Runtime dep for QtN(ear)F(ield)C(ommunications) ]
        ]]
"

qtconnectivity_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qtconnectivity_src_compile() {
    default

    option doc && emake docs
}

qtconnectivity_src_install() {
    default

    if option doc ; then
        dodoc doc/qt{bluetooth,nfc}.qch
        docinto html
        dodoc -r doc/qt{bluetooth,nfc}
    fi

    # remove references to build dir
    edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/$(exhost --target)/lib/libQt5*.prl
}

